import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/'

Vue.use(VueRouter)

const routerOptions = [
  {path: '/', component: 'Dashboard', name: "Dashboard", meta: {title: 'PPMI Staff Portal', requiresAuth: true}},
  {path: '/login', component: 'Login', name: "Login", meta: {title: 'Login - PPMI Staff Portal', requiresAuth: false}},
  {path: '/participant', component: 'ParticipantInfo', name: "Participant Info", meta: {title: 'Participant Info - PPMI Staff Portal', requiresAuth: true}},
  {path: '/upsits', component: 'UPSITs', name: "UPSIT Tasks", meta: {title: 'UPSIT Tasks - PPMI Staff Portal', requiresAuth: true}},
  {path: '/questions', component: 'Questions', name: "Pending Questions", meta: {title: 'Pending Questions - PPMI Staff Portal', requiresAuth: true}},
  {path: '/travel', component: 'TravelPlanning', name: "Travel Planning", meta: {title: 'Travel Planning - PPMI Staff Portal', requiresAuth: true}},
  {path: '/reports', component: 'Reports', name: "Reports", meta: {title: 'Reports - PPMI Staff Portal', requiresAuth: true}},
  {path: '/problems', component: 'Problems', name: "Problems", meta: {title: 'Problems - PPMI Staff Portal', requiresAuth: true}},
  {path: '*', redirect: "/"},
]

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`../views/${route.component}.vue`)
  };
});

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {

  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      document.title = to.name + " - PPMI Staff Portal";
      next()
      return
    }
     
    next({
      path: '/login',
      params: {nextUrl: to.fullPath}
    })
  } else {
    document.title = to.name + " - PPMI Staff Portal";
    next()
  }

});


export default router
