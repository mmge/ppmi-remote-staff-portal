export default {
    primary: "#E07800", //MJFF Orange
    secondary: '#263C49', //MJFF Dark Blue
    tertiary: '#495057',
    accent: '#3A91B7', //MJFF Light Blue
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107',
  }
  